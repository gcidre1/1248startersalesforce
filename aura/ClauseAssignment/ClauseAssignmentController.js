({
	
	doInit : function(component, event, helper) {
		
		//Set up column and modal column
		component.set("v.column",[ 
		{
			label: 'Assignment ID',
			fieldName: 'Name',
			type: 'text'
		},
		{
			label: 'Clause Name',
			fieldName: 'cName',
			type: 'text'
		},
		{
			label: 'Type',
			fieldName: 'Type',
			type: 'text'
		},
		{
			label: 'Action',
			type: 'button',
			typeAttributes: { label: 'Remove',
							  name: 'remove_detail',
							  title: 'Click to remove Record'}
		}]);
		
		component.set("v.modalColumn",[ 
		{
			label: 'Name',
			fieldName: 'Name',
			type: 'text'
		},
		{
			label: 'Type',
			fieldName: 'Type__c',
			type: 'text'
		},
		{
			label: 'Description',
			fieldName: 'Description__c',
			type: 'text'
		}]);
		
		component.set('v.selectedData', []);
		component.set('v.selectedModalData', []);
		
		//Setup test data
		helper.generateBaseData(component);
		
	},
	
	
	handleRemoveAction : function(component, event, helper){
		//get event and current row and delete row if remove button was pushed
		var action = event.getParam('action');
		var row = event.getParam('row');
		
		if(action.name == 'remove_detail') {
			//generate id list to remove
			var idList = [row.id];
			helper.removeAssignRecord(component, idList);
		}
	},
	
	handleRemoveButton : function(component, event, helper){
		//selected data should contain all rows that need to be deleted
		var rows = component.get('v.selectedData');
		var idList = [];
		for(var i = 0; i<rows.length; i++){
			idList.push(rows[i].id);
		}
		
		helper.removeAssignRecord(component, idList);
	},
	
	handleNewButton : function(component, event, helper){
		//Opens the window
		helper.generateModalData(component);
		component.set('v.isOpen', true);
	},
	
	handleNewAssignment : function(component, event, helper){
		//call assignment and close window
		helper.createAssignData(component);
		component.set('v.isOpen', false);
	},
	
	handleCancel : function(component, event, helper){
		//close the window
		component.set('v.isOpen', false);
	},
	
	updateSelectedData : function(component, event, helper){
		//close the window
		component.set('v.selectedData', event.getParam('selectedRows'));
	},
	
	updateSelectedModalData : function(component, event, helper){
		//close the window
		component.set('v.selectedModalData', event.getParam('selectedRows'));
	}
})