({

	//All the helper functions called here sets up all apex callback
	generateBaseData : function(component) {
	
		//get contract id
		var id = component.get("v.recordId");
		
		//get base data
		
		var action = component.get('c.getBaseData');
		action.setParams({recordId : id});
		
		//response of call back will contain the data
		action.setCallback(this, function(response){
		
			var state = response.getState();
			if(state === "SUCCESS"){
			
				//insert data to component
				component.set('v.data', response.getReturnValue());
			}
		});
		
		//Call Action in a queue
		$A.enqueueAction(action);
		
		
	},
	
	generateModalData : function(component) {
		//get modal data
		
		var action = component.get('c.getModalData');
		
		//response of call back will contain the data
		action.setCallback(this, function(response){
		
			var state = response.getState();
			if(state === "SUCCESS"){
			
				//insert data to component
				component.set('v.modalData', response.getReturnValue());
			}
		});
		
		//Call Action in a queue
		$A.enqueueAction(action);
	},
	
	createAssignData : function(component) {
		//get Contract id
		var id = component.get("v.recordId");
	
		//get selected assignments
		var rows = component.get('v.selectedModalData');
		var idList = [];
		for(var i = 0; i<rows.length; i++){
			idList.push(rows[i].Id);
		}
		
		
		
		//id list for assignment removal depends whether the remove button or remove selected button was click
		var action = component.get('c.createAssignment');
		
		action.setParams({recordId : id , clauseList : idList});
		
		//response of call back will contain the data
		action.setCallback(this, function(response){
		
			var state = response.getState();
			if(state === "SUCCESS"){
			
				//insert data to component
				component.set('v.data', response.getReturnValue());
			}
		});
		
		//Call Action in a queue
		$A.enqueueAction(action);
		
	},
	
	removeAssignRecord : function(component, idList){
	
		//get Contract id
		var id = component.get("v.recordId");
		
		//id list for assignment removal depends whether the remove button or remove selected button was click
		var action = component.get('c.removeAssignment');
		
		action.setParams({recordId : id , assignmentList : idList});
		
		//response of call back will contain the data
		action.setCallback(this, function(response){
		
			var state = response.getState();
			if(state === "SUCCESS"){
			
				//insert data to component
				component.set('v.data', response.getReturnValue());
			}
		});
		
		//Call Action in a queue
		$A.enqueueAction(action);
	}
	
})