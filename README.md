**This repository is suspose to accomplish the following:**

Below are the assignment details. Please let me know if you have any questions. It is preferred to use lightning.
Screening assignment
Introduction
Create a Developer Edition org from Salesforce for the purpose of this assignment. https://developer.salesforce.com/signup. You will be extending the Salesforce Standard Contract feature to include Contract Clauses and creating a custom user interface to apply multiples of Clauses to Contracts.
 Object Model
Create a custom object to contain the Contract Clauses. This will have a Clause Name, a Description field, and a Picklist field to Include the Type. Types are ‘Financial’, ‘Flexible Length’, and ‘Other’. Create a custom junction object to contain the Contract to Clause Assignments.  Take a screenshot of the Schema Builder to show the object model.
 
User Interface
Create a custom UI using either Visualforce or Lightning (Lightning preferred) that shows details from the Contract object and display Contract Clause assignments as a related list. Related listshould have ’New’ button and ’Remove’ as a record action. When you click on ’New’, it should pull up a list of clauses that the user can select multiple of. Once selected the Contract Clauseassignments need to be stored in the custom junction object and display that as a related record. When you click on ‘Remove’ action, it should disassociate the contract from the Contract Clause, deleting the junction object. The related list should be updated.
 
Apex Testing
Make sure that any Apex classes have 100% coverage. It should include both positive and negativetest cases.
 
Version Control
Create a Bitbucket.org account and a free public repository, then commit and push the object metadata, Schema Builder screenshot, and source code for this project. Then share the link to the repo with the screener.



**The resulting folder directory**

aura: Contain all lightning components
	->ClauseAssignment -> Full source code for component
	->ClauseAssignmentWithDetail -> calling Clause Assignment with displayDetail attribute set to true

classes: Contain all Apex and Test classes
	->ClauseAssignmentAPEXController.cls ->Controller
	->TestClauseAssignmentAPEXController.cls ->Test class

objects: Contain all custom object metadata
	->Contract_Clause_c.object -> Clause object
	->Contract_Clause_Assignment__c.object -> Junction object

**Object Model.jpg contains the Schema Builder screenshot.**