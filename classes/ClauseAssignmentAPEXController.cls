public with sharing class ClauseAssignmentAPEXController {
    //create wrapper for main table
    public class WrapClauseAssignment {
    	
    	@auraEnabled
    	public String id; //id of the clause assignment
    	
    	@auraEnabled
    	public String Name; //Clause Assignment auto-number
    	
    	@auraEnabled
    	public String cName; //Clause name
    	
    	@auraEnabled
    	public String Type; //type of Clause
    }
    
    @auraEnabled
    public static List<WrapClauseAssignment> getBaseData(ID recordId){
    
    	//get List of base data
    	List<Contract_Clause_Assignment__c> bData = [Select id, Name, Clause__r.Name, Clause__r.Type__c From Contract_Clause_Assignment__c where Contract__c = :recordId];
    
    	//pass data unto wrapper data
    	List<WrapClauseAssignment> result = new List<WrapClauseAssignment>();
    	for(Contract_Clause_Assignment__c assignData : bData){
    		WrapClauseAssignment wrap = new WrapClauseAssignment();
    		wrap.id = assignData.id;
    		wrap.Name = assignData.Name;
    		wrap.cName = assignData.Clause__r.Name;
    		wrap.Type = assignData.Clause__r.Type__c;
    		
    		result.add(wrap);
    	}
    	return result;
    }
    
    @auraEnabled
    public static List<Contract_Clause__c> getModalData(){
    	return [Select id, Name, Type__c, Description__c From Contract_Clause__c];
    }
    
    @auraEnabled
    public static List<WrapClauseAssignment> createAssignment(ID recordId, List<ID> clauseList){
    	
    	//generate clause assignments for each clause selected
    	List<Contract_Clause_Assignment__c> ccAssignList = new List<Contract_Clause_Assignment__c>();
    	
    	for(ID i : clauseList){
    		Contract_Clause_Assignment__c ccAssign = new Contract_Clause_Assignment__c();
    		ccAssign.Contract__c = recordId;
    		ccAssign.Clause__c = i;
    		ccAssignList.add(ccAssign);
    	}
    	
    	//insert bulk
    	insert ccAssignList;
    	
    	//generate new base data
    	return getBaseData(recordId);
    }
    
    @auraEnabled
    public static List<WrapClauseAssignment> removeAssignment(ID recordId, List<ID> assignmentList){
    	//create an assignment list containing id
    	List<Contract_Clause_Assignment__c> ccAssignList = new List<Contract_Clause_Assignment__c>();
    	
    	for(ID i : assignmentList){
    		Contract_Clause_Assignment__c ccAssign = new Contract_Clause_Assignment__c();
    		ccAssign.id = i;
    		ccAssignList.add(ccAssign);
    	}
    	
    	delete ccAssignList;
    	//generate new base data
    	return getBaseData(recordId);
    }
}