@isTest
public with sharing class TestClauseAssignmentAPEXController {
    //Have a test setup method followed by series of positive and negative tests
    @testSetup
    static void generateData(){
    	//create account for contract
    	Account testAccount = new Account();
    	testAccount.name = 'testAccount';
    	insert testAccount;
    	
    	//insert a contract followed by contract clauses
    	Contract testContract = new Contract();
    	testContract.accountId = testAccount.id;
    	testContract.StartDate = Date.today();
    	testContract.ContractTerm = 5;
    	testContract.Status = 'Draft';
    	insert testContract;
    	
    	//create three clauses
    	Contract_Clause__c cClause1 = new Contract_Clause__c();
    	cClause1.name = 'testFinancialClause';
    	cClause1.Type__c = 'Financial';
    	cClause1.Description__c = '1';
    	insert cClause1;
    	
    	Contract_Clause__c cClause2 = new Contract_Clause__c();
    	cClause2.name = 'testFlexibleClause';
    	cClause2.Type__c = 'Flexible Length';
    	cClause2.Description__c = '2';
    	insert cClause2;
    	
    	Contract_Clause__c cClause3 = new Contract_Clause__c();
    	cClause3.name = 'testOtherClause';
    	cClause3.Type__c = 'Other';
    	cClause3.Description__c = '3';
    	insert cClause3;
    	
    	//Generate two clause Assignment
    	Contract_Clause_Assignment__c ccAs = new Contract_Clause_Assignment__c();
    	ccAs.Contract__c = testContract.id;
    	ccAs.Clause__c = cClause1.id;
    	
    	insert ccAs;
    	
    	Contract_Clause_Assignment__c ccAs2 = new Contract_Clause_Assignment__c();
    	ccAs2.Contract__c = testContract.id;
    	ccAs2.Clause__c = cClause2.id;
    	
    	insert ccAs2;
    }
    
    
    //Positive test functions for each controller function
    @isTest
    static void getBaseDataPositiveTest(){
    	
    	//get record id of contract
    	Contract testContract = [Select id from Contract];
    	ID recordId = testContract.id;
    	
    	//get base data
    	List<ClauseAssignmentAPEXController.WrapClauseAssignment> baseData
    		= ClauseAssignmentAPEXController.getBaseData(recordId);
    	
    	// should be two clause assignments in there
    	system.assertEquals(baseData.size(), 2, 'Size does not match');
    	
    	system.assertEquals(baseData[0].cName, 'testFinancialClause', 'Index 0 doesnt Match');
    	system.assertEquals(baseData[0].Type, 'Financial', 'Index 0 type doesnt Match');
    	
    	system.assertEquals(baseData[1].cName, 'testFlexibleClause', 'Index 1 doesnt Match');
		system.assertEquals(baseData[1].Type, 'Flexible Length', 'Index 1 type doesnt Match');
		    	
    }
    
    @isTest
    static void getModalDataPositiveTest(){
    	
    	//get modal data
    	List<Contract_Clause__c> modalData
    		= ClauseAssignmentAPEXController.getModalData();
    	
    	//Verify the three clause in the list
    	system.assertEquals(modalData.size(), 3, 'Incorrect size for modal');
    	
    	system.assertEquals(modalData[0].name, 'testFinancialClause', 'index 0 name mismatch');
    	system.assertEquals(modalData[0].Type__c, 'Financial', 'index 0 type mismatch');
    	system.assertEquals(modalData[0].Description__c, '1', 'index 0 Desc mismatch');
    	
    	system.assertEquals(modalData[1].name, 'testFlexibleClause', 'index 1 name mismatch');
    	system.assertEquals(modalData[1].Type__c, 'Flexible Length', 'index 1 type mismatch');
    	system.assertEquals(modalData[1].Description__c, '2', 'index 1 Desc mismatch');
    	
    	system.assertEquals(modalData[2].name, 'testOtherClause', 'index 2 name mismatch');
    	system.assertEquals(modalData[2].Type__c, 'Other', 'index 2 type mismatch');
    	system.assertEquals(modalData[2].Description__c, '3', 'index 2 Desc mismatch');
    	
    }
    
    @isTest
    static void createAssignmentPositiveTest(){
    	
    	//get record id of contract
    	Contract testContract = [Select id from Contract];
    	ID recordId = testContract.id;
    	
    	//get record id of clause
    	Contract_Clause__c testClause = [select id from Contract_Clause__c where Type__c = 'Other'];
    	List<ID> clauseList = new List<ID>();
    	clauseList.add(testClause.id);
    	
    	//Get base data result from creating
    	List<ClauseAssignmentAPEXController.WrapClauseAssignment> baseData
    		= ClauseAssignmentAPEXController.createAssignment(recordId, clauseList);
    	
    	//Verify the three assessment results in baseData
    	system.assertEquals(baseData.size(), 3, 'Size does not match');
    	
    	system.assertEquals(baseData[0].cName, 'testFinancialClause', 'Index 0 doesnt Match');
    	system.assertEquals(baseData[0].Type, 'Financial', 'Index 0 type doesnt Match');
    	
    	system.assertEquals(baseData[1].cName, 'testFlexibleClause', 'Index 1 doesnt Match');
		system.assertEquals(baseData[1].Type, 'Flexible Length', 'Index 1 type doesnt Match');
    	
    	system.assertEquals(baseData[2].cName, 'testOtherClause', 'Index 2 doesnt Match');
		system.assertEquals(baseData[2].Type, 'Other', 'Index 2 type doesnt Match');
    	
    	
    	
    }
    
    @isTest
    static void removeAssignmentPositiveTest(){
    	
    	//get record id of contract
    	Contract testContract = [Select id from Contract];
    	ID recordId = testContract.id;
    	
    	//get a clause assignment id
    	Contract_Clause_Assignment__c testccAssign = [select id from Contract_Clause_Assignment__c where Clause__r.Name = 'testFlexibleClause'];
    	List<ID> ccAssignList = new List<ID>();
    	ccAssignList.add(testccAssign.id);
    	
    	//get wrapper data
    	List<ClauseAssignmentAPEXController.WrapClauseAssignment> baseData
    		= ClauseAssignmentAPEXController.removeAssignment(recordId, ccAssignList);
    		
    	//Should have only one assignment
    	system.assertEquals(baseData.size(), 1, 'Size does not match');
    	
    	system.assertEquals(baseData[0].cName, 'testFinancialClause', 'Index 0 doesnt Match');
    	system.assertEquals(baseData[0].Type, 'Financial', 'Index 0 type doesnt Match');
    	
    }
    
    //two Negative test functions for those with improper ids
    @isTest
    static void createAssignmentNegativeTest(){
    	String message;
    	try{
    		List<ID> idList = new List<ID>();
    		idList.add(null);
    		ClauseAssignmentAPEXController.createAssignment(null, idList);
    	}
    	catch(exception e){
    		message = e.getMessage();
    	}
    	
    	//TODO: insert assert statement here
    	system.assertEquals(message,
    		'Insert failed. First exception on row 0; first error: REQUIRED_FIELD_MISSING, Required fields are missing: [Contract__c, Clause__c]: [Contract__c, Clause__c]');
    }
    
    @isTest
    static void removeAssignmentNegativeTest(){
    	String message;
    	try{
    		List<ID> idList = new List<ID>();
    		idList.add(null);
    		ClauseAssignmentAPEXController.removeAssignment(null, idList);
    	}
    	catch(exception e){
    		message = e.getMessage();
    	}
    	
    	//TODO: insert assert statement here
    	system.assertEquals(message,
    		'Invalid id at index 0: null');
    }
}